
FROM composer:2 as composer
FROM php:8.2-fpm-alpine as base

# Necessary tools
RUN apk add --update --no-cache ${PHPIZE_DEPS} git curl

# Symfony CLI tool
RUN apk add --no-cache bash && \
    curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.alpine.sh' | bash && \
    apk add symfony-cli && \
    apk del bash

# ZIP module
RUN apk add --no-cache libzip-dev && docker-php-ext-configure zip && docker-php-ext-install zip

# Imagick module
RUN apk add --no-cache libgomp imagemagick imagemagick-dev && \
	pecl install -o -f imagick && \
	docker-php-ext-enable imagick

# MySQL client library (mysqli extension is bundled with PHP)
RUN docker-php-ext-install mysqli pdo pdo_mysql

# ICU library for intl extension
RUN apk add --no-cache icu-dev icu-libs && \
    docker-php-ext-configure intl && \
    docker-php-ext-install intl

# Composer
COPY --from=composer /usr/bin/composer /usr/local/bin/composer

# PHP Extensions
RUN docker-php-ext-install \
        pdo pdo_mysql \
    && pecl install apcu && docker-php-ext-enable apcu

RUN apk del --no-cache ${PHPIZE_DEPS}

# Clean up image
RUN rm -rf /tmp/* /var/cache

